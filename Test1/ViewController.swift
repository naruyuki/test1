//
//  ViewController.swift
//  Test1
//
//  Created by 中越成幸 on 2018/11/07.
//  Copyright © 2018 中越成幸. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBOutlet weak var textLabel: UILabel!
    
    @IBAction func pushButton(_ sender: Any) {
        textLabel.text = "Hello,World!!"
    }
    
}

